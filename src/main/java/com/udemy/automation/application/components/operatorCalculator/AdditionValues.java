package com.udemy.automation.application.components.operatorCalculator;

import lombok.Setter;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.springframework.beans.factory.annotation.Autowired;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

/**
 * @author Henry J. Calani A.
 */
public class AdditionValues implements Task {

    @Setter
    private String firstValue;

    @Setter
    private String secondValue;

    @Autowired
    private AdditionComponent component;

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                component.firstValue(firstValue),
                component.selectOperator(),
                component.secondValue(secondValue),
                component.pushResultButton(),
                WaitUntil.the(component.getHeaderTitle(),isVisible()).forNoMoreThan(50).seconds()
        );
    }
}
