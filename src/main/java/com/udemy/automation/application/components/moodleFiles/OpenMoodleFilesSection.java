package com.udemy.automation.application.components.moodleFiles;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Henry J. Calani A.
 */
public class OpenMoodleFilesSection implements Task {

    @Autowired
    private MoodleFilesComponent moodleFilesComponent;

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
               // WaitUntil.the(notificationsComponent.getHeaderTitle(),isVisible()).forNoMoreThan(30).seconds(),
                moodleFilesComponent.openNotification()
        );
    }
}
