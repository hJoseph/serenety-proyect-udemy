package com.udemy.automation.application.components.operatorCalculator;


/**
 * @author Henry J. Calani A.
 */
public class ConstantsCalculator {

    public  static final String TITLE_PAGE = "Title Page web";
    public  static final String TITLE = "//h3[.='Super Calculator']";
    public  static final String FIRST_VALUE_TITLE = "First Value";
    public  static final String FIRST_VALUE = "//input[1]";
    public  static final String SECOND_VALUE_TITLE = "Second Value";
    public  static final String SECOND_VALUE = "//input[2]";
    public  static final String SUBMIT_VALUE_TITLE = "Submit Value";
    public  static final String INPUT_SUBMIT_SELECTOR = "//button[@id='gobutton']";
    public  static final String OPERATOR = "Select Operator";
    public  static final String OPERATOR_SELECTED = "Select Operator";
    public  static final String OPERATATION_ADDITION = "body > div > div > form > select > option:nth-child(1)";
    public  static final String OPERATION_SUBSTRACTION = "body > div > div > form > select > option:nth-child(5)";
    public  static final String OPERATATION_MULTIPLICATION = "body > div > div > form > select > option:nth-child(4)";
    public  static final String OPERATATION_DIVISION = "body > div > div > form > select > option:nth-child(2)";
}
