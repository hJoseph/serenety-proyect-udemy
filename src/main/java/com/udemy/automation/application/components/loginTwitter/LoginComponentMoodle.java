package com.udemy.automation.application.components.loginTwitter;

import com.udemy.automation.application.framework.context.PrototypeScope;
import lombok.Getter;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import javax.annotation.PostConstruct;

/**
 * @author Henry J. Calani A.
 */
@PrototypeScope
class LoginComponentMoodle {

    @Getter
    private Target headerTitle;

    @Getter
    private Target emailInput;

    @Getter
    private Target passwordInput;

    @Getter
    private Target loginButton;

    Performable enterEmailValue(String value) {
        return Enter.theValue(value).into(emailInput).thenHit(Keys.TAB);
    }

    Performable enterPasswordValue(String value) {
        return Enter.theValue(value).into(passwordInput).thenHit(Keys.TAB);
    }

    Performable pushLoginButton() {

        return Click.on(loginButton);
    }

    @PostConstruct
    void onPostConstruct() {

        headerTitle = Target.the(ConstantsMoodle.TITLE_PAGE).located(By.xpath(ConstantsMoodle.TITLE));
        emailInput = Target.the(ConstantsMoodle.EMAIL_LABEL_INPUT).located(By.id(ConstantsMoodle.EMAIL_SELECTOR_INPUT));
        passwordInput = Target.the(ConstantsMoodle.PASSWORD_LABEL_INPUT).located(By.id(ConstantsMoodle.PASSWORD_SELECTOR_INPUT));
        loginButton = Target.the(ConstantsMoodle.INPUT_SUBMIT).located(By.id(ConstantsMoodle.INPUT_SUBMIT_SELECTOR));

    }
}
