package com.udemy.automation.application.components.loginTwitter;


/**
 * @author Henry J. Calani A.
 */
public class ConstantsMoodle {

    public  static final String TITLE_PAGE = "Title Page web";
    public  static final String TITLE = "//div[@class='mt-3']";

    public  static final String EMAIL_LABEL_INPUT = "Email input";
    public  static final String EMAIL_SELECTOR_INPUT = "username";

    public  static final String PASSWORD_LABEL_INPUT = "Password label";
    public  static final String PASSWORD_SELECTOR_INPUT = "password";
    
    public  static final String INPUT_SUBMIT = "Login button";
    public  static final String INPUT_SUBMIT_SELECTOR = "loginbtn";
}
