package com.udemy.automation.application.components.login;

import lombok.Setter;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import org.springframework.beans.factory.annotation.Autowired;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

/**
 * @author Henry J. Calani A.
 */
public class InsertCredentialsAndLogin implements Task {

    @Setter
    private String email;

    @Setter
    private String password;

    @Autowired
    private LoginComponent component;

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                WaitUntil.the(component.getHeaderTitle(),isVisible()).forNoMoreThan(30).seconds(),
                component.enterEmailValue(email),
                component.enterPasswordValue(password),
                component.pushLoginButton()
        );
    }
}

/*

  actor.attemptsTo(
                component.enterEmailValue(email),
                component.enterPasswordValue(password),
                component.pushLoginButton(),
                WaitUntil.the(component.getHeaderTitle(),isVisible()).forNoMoreThan(50).seconds()
        );


 */


