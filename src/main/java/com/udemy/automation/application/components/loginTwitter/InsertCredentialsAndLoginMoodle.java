package com.udemy.automation.application.components.loginTwitter;

import lombok.Setter;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.springframework.beans.factory.annotation.Autowired;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

/**
 * @author Henry J. Calani A.
 */
public class InsertCredentialsAndLoginMoodle implements Task {

    @Setter
    private String email;

    @Setter
    private String password;

    @Autowired
    private LoginComponentMoodle component;

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                WaitUntil.the(component.getHeaderTitle(),isVisible()).forNoMoreThan(30).seconds(),
                component.enterEmailValue(email),
                component.enterPasswordValue(password),
                component.pushLoginButton()
        );
    }
}
