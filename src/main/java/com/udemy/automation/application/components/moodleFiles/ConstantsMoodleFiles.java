package com.udemy.automation.application.components.moodleFiles;

/**
 * @author Henry J. Calani A.
 */
public class ConstantsMoodleFiles {

    public static final String LABEL_HEADER= "Title of notification";
    public static final String ICON_HEADER = "//span[@class='site-name d-none d-md-inline']";

    public static final String LABEL_NOTIFICATION= "Notification";
    public static final String ICON_NOTIFICATION = "#nav-drawer > nav > a:nth-child(4) > div > div > span.media-body";



}
