package com.udemy.automation.application.components.moodleFiles;

import com.udemy.automation.application.framework.context.PrototypeScope;
import lombok.Getter;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

import javax.annotation.PostConstruct;

/**
 * @author Henry J. Calani A.
 */
@PrototypeScope
class MoodleFilesComponent {

    @Getter
    private Target headerTitle;

    @Getter
    private Target clickNotification;

    Performable openNotification() {

        return Click.on(clickNotification);
    }

    @PostConstruct
    void onPostConstruct() {
         headerTitle = Target.the(ConstantsMoodleFiles.LABEL_HEADER).located(By.xpath(ConstantsMoodleFiles.ICON_HEADER));
         clickNotification = Target.the(ConstantsMoodleFiles.LABEL_NOTIFICATION).located(By.cssSelector(ConstantsMoodleFiles.ICON_NOTIFICATION));

    }
}
