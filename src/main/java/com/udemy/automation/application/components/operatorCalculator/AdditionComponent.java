package com.udemy.automation.application.components.operatorCalculator;

import com.udemy.automation.application.framework.context.PrototypeScope;
import lombok.Getter;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import javax.annotation.PostConstruct;

/**
 * @author Henry J. Calani A.
 */
@PrototypeScope
class AdditionComponent {

    @Getter
    private Target headerTitle;

    @Getter
    private Target firstValueInput;

    @Getter
    private Target operator;

    @Getter
    private Target secondValueInput;

    @Getter
    private Target submitButton;

    Performable firstValue(String value) {

        return Enter.theValue(value).into(firstValueInput).thenHit(Keys.TAB);
    }

    Performable secondValue(String value) {

        return Enter.theValue(value).into(secondValueInput).thenHit(Keys.TAB);
    }

    Performable selectOperator() {
      return Click.on(operator);
    }

    Performable pushResultButton() {

        return Click.on(submitButton);
    }

    @PostConstruct
    void onPostConstruct() {
        headerTitle = Target.the(ConstantsCalculator.TITLE_PAGE).located(By.xpath(ConstantsCalculator.TITLE));
        firstValueInput = Target.the(ConstantsCalculator.FIRST_VALUE_TITLE).located(By.xpath(ConstantsCalculator.FIRST_VALUE));
        operator = Target.the(ConstantsCalculator.OPERATOR).located(By.cssSelector(ConstantsCalculator.OPERATATION_ADDITION));
        secondValueInput = Target.the(ConstantsCalculator.SECOND_VALUE_TITLE).located(By.xpath(ConstantsCalculator.SECOND_VALUE));
        submitButton = Target.the(ConstantsCalculator.SUBMIT_VALUE_TITLE).located(By.xpath(ConstantsCalculator.INPUT_SUBMIT_SELECTOR));

    }
}
