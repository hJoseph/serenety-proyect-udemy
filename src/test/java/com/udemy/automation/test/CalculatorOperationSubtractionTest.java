package com.udemy.automation.test;

import com.udemy.automation.application.components.browser.OpenBrowserCalculator;
import com.udemy.automation.application.components.operatorCalculator.SubtractionValues;
import com.udemy.automation.application.config.ApplicationConfig;
import net.serenitybdd.screenplay.Actor;
import net.thucydides.core.annotations.WithTag;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static net.serenitybdd.screenplay.GivenWhenThen.givenThat;
import static net.serenitybdd.screenplay.GivenWhenThen.then;

/**
 * @author Henry J. Calani A.
 */
public class CalculatorOperationSubtractionTest extends AbstractTest {

    @Autowired
    private ApplicationConfig config;

    private Actor actor = Actor.named("henry");

    private OpenBrowserCalculator openBrowser;

    private SubtractionValues operationValues;

    @Before
    public void setup() {
        initializeActor(actor);
        openBrowser = taskInstance(OpenBrowserCalculator.class);
        operationValues = taskInstance(SubtractionValues.class);

        operationValues.setFirstValue(config.getFirstValue());
        operationValues.setSecondValue(config.getSecondValue());

    }

    @WithTag("subtraction")
    @Test
    public void operatorSubtraction() {
        givenThat(actor)
                .attemptsTo(openBrowser);
        then(actor)
                .attemptsTo(operationValues);
    }
}
