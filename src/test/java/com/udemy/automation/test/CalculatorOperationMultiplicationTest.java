package com.udemy.automation.test;

import com.udemy.automation.application.components.browser.OpenBrowserCalculator;
import com.udemy.automation.application.components.operatorCalculator.MultiplicationValues;
import com.udemy.automation.application.config.ApplicationConfig;
import net.serenitybdd.screenplay.Actor;
import net.thucydides.core.annotations.WithTag;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static net.serenitybdd.screenplay.GivenWhenThen.givenThat;
import static net.serenitybdd.screenplay.GivenWhenThen.then;

/**
 * @author Henry J. Calani A.
 */
public class CalculatorOperationMultiplicationTest extends AbstractTest {

    @Autowired
    private ApplicationConfig config;

    private Actor actor = Actor.named("henry");

    private OpenBrowserCalculator openBrowser;

    private MultiplicationValues operationValues;

    @Before
    public void setup() {
        initializeActor(actor);
        openBrowser = taskInstance(OpenBrowserCalculator.class);
        operationValues = taskInstance(MultiplicationValues.class);

        operationValues.setFirstValue(config.getFirstValue());
        operationValues.setSecondValue(config.getSecondValue());

    }

    @WithTag("multiplication")
    @Test
    public void operatorMultiplication() {
        givenThat(actor)
                .attemptsTo(openBrowser);
        then(actor)
                .attemptsTo(operationValues);
    }
}
