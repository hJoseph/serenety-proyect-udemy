package com.udemy.automation.test;

import com.udemy.automation.application.components.browser.OpenBrowserMoodle;
import com.udemy.automation.application.components.loginTwitter.InsertCredentialsAndLoginMoodle;
import com.udemy.automation.application.components.moodleFiles.OpenMoodleFilesSection;
import com.udemy.automation.application.config.ApplicationConfig;
import net.serenitybdd.screenplay.Actor;
import net.thucydides.core.annotations.WithTag;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static net.serenitybdd.screenplay.GivenWhenThen.*;

/**
 * @author Henry J. Calani A.
 */
public class OpenProjectMoodleTest extends AbstractTest {

    @Autowired
    private ApplicationConfig config;

    private Actor actor = Actor.named("henry");

    private OpenBrowserMoodle openBrowser;

    private InsertCredentialsAndLoginMoodle insertCredentialsAndLogin;

    private OpenMoodleFilesSection openNotification;



    @Before
    public void setup() {
        initializeActor(actor);

        openBrowser = taskInstance(OpenBrowserMoodle.class);

        insertCredentialsAndLogin = taskInstance(InsertCredentialsAndLoginMoodle.class);
        insertCredentialsAndLogin.setEmail(config.getUserMoodle());
        insertCredentialsAndLogin.setPassword(config.getPasswordMoodle());
        openNotification = taskInstance(OpenMoodleFilesSection.class);
    }

    @WithTag("openProyect")
    @Test
    public void userLoginMoodle() {
        givenThat(actor)
                .attemptsTo(openBrowser);
        when(actor)
                .attemptsTo(insertCredentialsAndLogin);
        then(actor)
                .attemptsTo(openNotification);
    }
}
